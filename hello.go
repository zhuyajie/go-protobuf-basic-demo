package main

import (
	"fmt"
	"log"

	"github.com/golang/protobuf/proto"
	pb "./proto/example"
)

func main() {
	// 创建一条pb消息实体
	// 注意 用到的是Test实体的指针
	test := &pb.Test{
		Label: proto.String("hello"),
		Type:  proto.Int32(17),
		Reps:  []int64{1, 2, 3},
		// 复杂结构类型字段使用下划线逐级分隔字段名作为结构体名称
		Optionalgroup: &pb.Test_OptionalGroup{
			RequiredField: proto.String("good bye"),
		},
		Union: &pb.Test_Name{"fred"},
		// 内嵌消息
		Info: &pb.SomeInfo{
			Title:[]byte("a title"),  // []byte 类型无需转换,可直接使用go的数据
		},
	}
	// 序列化消息实体
	data, err := proto.Marshal(test)
	if err != nil {
		log.Fatal("marshaling error: ", err)
	}

	// 创建一个消息实体零值
	newTest := &pb.Test{}
	// 将pb消息反序列化为消息实体
	err = proto.Unmarshal(data, newTest)
	if err != nil {
		log.Fatal("unmarshaling error: ", err)
	}

	// Now test and newTest contain the same data.
	if test.GetLabel() != newTest.GetLabel() {
		log.Fatalf("data mismatch %q != %q", test.GetLabel(), newTest.GetLabel())
	}
	// union 类型 需要使用 switch 确定数据类型
	switch u := test.Union.(type) {
	case *pb.Test_Number: // u.Number contains the number.
		fmt.Println(u.Number)
	case *pb.Test_Name: // u.Name contains the string.
		fmt.Println(u.Name)
	}
	fmt.Println(string(test.Info.Title))
	// enum 数据 FOO的元素值使用常量FOO_name引用:
	fmt.Printf("%d",pb.FOO_X)

	// 根据enum元素的值返回enum元素名
	name :=proto.EnumName(pb.FOO_name,17)
	fmt.Println(name)
}

